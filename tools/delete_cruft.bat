deltree /y CVS
erase .cvsignore
erase ABOUT-NLS
erase Makefile*
erase .#*
erase *.h
erase *.c
erase *.m4
deltree /y archive
erase *.in
erase *.sh
erase configure.*
erase *-sh
erase *.6
erase *.ico
erase *.ico
erase *.rc
erase *~
erase missing
erase mkinstalldirs
deltree /y help\cvs
deltree /y icons\cvs
deltree /y intl
deltree /y libpng-1.2.5
deltree /y messages\cvs
deltree /y messages\ca
rem deltree /y messages\ca\cvs
deltree /y modules
deltree /y old.Makefiles
deltree /y opening\cvs
deltree /y po
deltree /y zlib
erase *.mak
deltree /y lib
deltree /y debug
erase *.ncb
erase *.mdp
erase *.plg
erase *.dsp
deltree /y SAVED_GAMES
erase *.opt
deltree /y release
erase *.diff
erase *.dsw
deltree /y m4
erase depcomp
erase config.guess
erase config.sub
erase debug.txt
erase *.out
deltree /y autom4te.cache
erase *.ini
erase *.aps
erase *.junk
erase lincity
erase xlincity
deltree /y locale
deltree /y gettext-w32
deltree /y autopackage
erase lincity.png
erase lincity.desktop
deltree /y doc

rem -------------------------------------------------
rem This one must be last, because it ends batch file
deltree /y tools
